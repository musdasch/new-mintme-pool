module github.com/etclabscore/open-etc-pool

go 1.15

require (
	github.com/btcsuite/btcd v0.21.0-beta // indirect
	github.com/edsrzf/mmap-go v1.0.0
	github.com/etclabscore/go-etchash v0.0.0-20210517131846-9a3cc202249e
	github.com/ethereum/go-ethereum v1.10.3
	github.com/garyburd/redigo v1.6.2 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/golang-lru v0.5.5-0.20210104140557-80c98217689d
	github.com/mintme-com/cpuid v1.3.1 // indirect
	github.com/webchain-network/cryptonight v0.0.0-20210212081201-c3f06436384f // indirect
	github.com/webchain-network/webchaind v0.9.0 // indirect
	github.com/yvasiyarov/go-metrics v0.0.0-20150112132944-c25f46c4b940 // indirect
	github.com/yvasiyarov/gorelic v0.0.7
	github.com/yvasiyarov/newrelic_platform_go v0.0.0-20160601141957-9c099fbc30e9 // indirect
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
	golang.org/x/sys v0.0.0-20210514084401-e8d321eab015 // indirect
	gopkg.in/bsm/ratelimit.v1 v1.0.0-20160220154919-db14e161995a // indirect
	gopkg.in/redis.v3 v3.6.4
)

//replace github.com/klauspost/cpuid v0.0.0-20170728055534-ae7887de9fa5 => github.com/mintme-com/cpuid v1.3.1

replace github.com/mintme-com/cpuid v1.3.1 => github.com/klauspost/cpuid v0.0.0-20170728055534-ae7887de9fa5

replace github.com/webchain-network/webchaind/accounts/abi/bind v0.0.0-20190521151733-fe17e9e1e2ce => github.com/eth-classic/go-ethereum/accounts/abi/bind v0.0.0-20190524195439-fe17e9e1e2ce

//replace github.com/webchain-network/webchaind v0.9.0 => github.com/ethereum/go-ethereum v1.10.3
